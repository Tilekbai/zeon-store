from django.db import (
    models,
)
from django.urls import (
    reverse,
)


class Department(models.Model):
    title = models.CharField(max_length=100, unique=True)

    def get_absolute_url(self):
        return reverse("department", args=[self.id])

    def __str__(self):
        return self.title

class Product(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    delivery_date = models.DateField(auto_now_add=True)
    disposal_date = models.DateField()
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse(
            "product-update", args=[str(self.department.id), str(self.id)]
        )

    def __str__(self):
        return f"{self.title}: due {self.disposal_date}"

    class Meta:
        ordering = ["disposal_date"]