from django.urls import (
    reverse,
    reverse_lazy,
)
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView,
)
from .models import (
    Department,
    Product,
)


class DepartmentListView(ListView):
    model = Department
    template_name = "index.html"

class ProductListView(ListView):
    model = Product
    template_name = "product_list.html"

    def get_queryset(self):
        return Product.objects.filter(department_id=self.kwargs["department_id"])

    def get_context_data(self):
        context = super().get_context_data()
        context["department"] = Department.objects.get(id=self.kwargs["department_id"])
        return context

class DepartmentCreate(CreateView):
    model = Department
    fields = ["title"]

    def get_context_data(self):
        context = super(DepartmentCreate, self).get_context_data()
        context["title"] = "Add a new department"
        return context

class ProductCreate(CreateView):
    model = Product
    fields = [
        "department",
        "title",
        "description",
        "disposal_date",
    ]

    def get_initial(self):
        initial_data = super(ProductCreate, self).get_initial()
        department = Department.objects.get(id=self.kwargs["department_id"])
        initial_data["department"] = department
        return initial_data

    def get_context_data(self):
        context = super(ProductCreate, self).get_context_data()
        department = Department.objects.get(id=self.kwargs["department_id"])
        context["department"] = department
        context["title"] = "Create a new product"
        return context

    def get_success_url(self):
        return reverse("department", args=[self.object.department_id])

class ProductUpdate(UpdateView):
    model = Product
    fields = [
        "department",
        "title",
        "description",
        "disposal_date",
    ]

    def get_context_data(self):
        context = super(ProductUpdate, self).get_context_data()
        context["department"] = self.object.department
        context["title"] = "Edit product"
        return context

    def get_success_url(self):
        return reverse("department", args=[self.object.department_id])

class DepartmentDelete(DeleteView):
    model = Department
    success_url = reverse_lazy("index")

class ProductDelete(DeleteView):
    model = Product

    def get_success_url(self):
        return reverse_lazy("department", args=[self.kwargs["department_id"]])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["department"] = self.object.department
        return context