from django.contrib import (
    admin,
)
from store_app.models import (
    Department,
    Product,
)

admin.site.register(Department)
admin.site.register(Product)