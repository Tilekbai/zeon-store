from django.urls import path
from . import views

urlpatterns = [
    path("", views.DepartmentListView.as_view(), name="index"),
    path("department/<int:department_id>/", views.ProductListView.as_view(), name="department"),
    path("department/add/", views.DepartmentCreate.as_view(), name="department-add"),
    path("department/<int:department_id>/product/add/", views.ProductCreate.as_view(), name="product-add"),
    path("department/<int:department_id>/product/<int:pk>/", views.ProductUpdate.as_view(), name="product-update"),
    path("department/<int:pk>/delete/", views.DepartmentDelete.as_view(), name="department-delete"),
    path("department/<int:department_id>/product/<int:pk>/delete/", views.ProductDelete.as_view(), name="product-delete"),
]